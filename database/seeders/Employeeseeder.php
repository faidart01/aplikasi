<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('employees')->insert([
            'nama' => 'fawaid',
            'jeniskelamin' => 'cowo',
            'notelepon' => '03938377212',
        ]);
    }
}
